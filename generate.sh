#!/bin/sh
PROTO_ROOT="proto"
DIRS=$(echo $PROTO_ROOT/*/)

for dir in $DIRS; do
  if echo $dir | grep -q google; then
    continue
  fi

  for proto in $(find $dir -name "*.proto"); do
    protoc -I=proto/ \
              --go_out=./gen/go \
              --go_opt=paths=source_relative \
              --go-grpc_out=./gen/go \
              --go-grpc_opt=paths=source_relative \
              --grpc-gateway_out=./gen/go \
              --grpc-gateway_opt generate_unbound_methods=true \
              --grpc-gateway_opt=paths=source_relative \
              --openapiv2_out=./gen/go \
              $proto
  done
done